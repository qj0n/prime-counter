﻿using System.Collections.Generic;
using System.Linq;
using PrimeCounter.Lib.Interfaces;

namespace PrimeCounter.Lib.Sieve
{
    public class SieveCounter : IPrimeCounter
    {
        private readonly IPrimesGenerator _generator;

        public SieveCounter(IPrimesGenerator generator)
        {
            _generator = generator;
        }

        public int CountPrimes(int min, int max)
        {
            var primes = _generator.Sieve(max);
            return primes.SkipWhile(i => i < min).Count();
        }
    }
}
