using System.Collections.Generic;
using System.Linq;
using PrimeCounter.Lib.Interfaces;

namespace PrimeCounter.Lib.Sieve
{
    public class SieveGenerator : IPrimesGenerator
    {
        // based on http://csharphelper.com/blog/2014/08/use-the-sieve-of-eratosthenes-to-find-prime-numbers-in-c/
        // author: Rod Stephens
        public IEnumerable<int> Sieve(int max)
        {
            var resultPrimes = new List<int>();
            bool[] isPrime = Enumerable.Repeat(true, max).ToArray();

            // Cross out multiples.
            for (int i = 2; i < max; i++)
            {
                // See if i is prime.
                if (isPrime[i])
                {
                    // Knock out multiples of i.
                    for (int j = i * 2; j < max; j += i)
                        isPrime[j] = false;
                    resultPrimes.Add(i);
                }
            }

            return resultPrimes;
        }
    }
}