namespace PrimeCounter.Lib.Interfaces
{
    public interface IPrimeCounter
    {
        int CountPrimes(int min, int max);
    }
}