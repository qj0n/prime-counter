﻿namespace PrimeCounter.Lib.Interfaces
{
    public interface IPrimeChecker
    {
        bool IsPrime(int n);
    }
}