﻿using System;
using PrimeCounter.Lib.Interfaces;

namespace PrimeCounter.Lib.Checker
{
    public class PrimeChecker : IPrimeChecker
    {
        public bool IsPrime(int n)
        {
            if (n < 2)
                return false;

            int maxDivisor = (int)Math.Sqrt(n);

            for (int i = 2; i <= maxDivisor; i++)
                if (n % i == 0)
                    return false;

            return true;
        }
    }
}
