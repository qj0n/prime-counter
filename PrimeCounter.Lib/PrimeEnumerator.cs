﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PrimeCounter.Lib.Checker;

namespace PrimeCounter.Lib
{
    public static class PrimeEnumerator 
    {
        public static IEnumerable<int> PrimeRange(int min, int max)
        {
            var checker = new PrimeChecker();
            for (int i = min; i <= max; i++)
            {
                if(checker.IsPrime(i))
                    yield return i;
            }
        }
    }

    internal class EnumerableCache<T> : IEnumerable<T>
    {
        private IEnumerator<T> _cachee;
        private readonly List<T> collectionChache = new List<T>();

        public EnumerableCache(IEnumerable<T> cachee)
        {
            _cachee = cachee.GetEnumerator();
        }

        public IEnumerator<T> GetEnumerator()
        {
            using (_cachee)
            {
                for (var index = 0;; index++)
                {
                    if (collectionChache.Count > index)
                    {
                        var element = collectionChache[index];
                        yield return element;
                    }
                    else if (_cachee?.MoveNext() ?? false)
                    {
                        collectionChache.Add(_cachee.Current);
                        yield return collectionChache.Last();
                    }
                    else
                    {
                        break;
                    }
                }
            }
            _cachee = null;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public static class EnumerableExtensions
    {
        public static IEnumerable<T> Cache<T>(this IEnumerable<T> cachee)
        {
            return new EnumerableCache<T>(cachee);
        }
    }
}
